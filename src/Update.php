<?php

class Update
{
    CONST MANIFEST_URL = 'https://launchermeta.mojang.com/mc/game/version_manifest.json';
    CONST DOWNLOAD_URL = 'https://s3.amazonaws.com/Minecraft.Download/versions/__VER__/minecraft_server.__VER__.jar';

    protected $snapshot;
    protected $release;

    protected $config;
    protected $current;

    public function __construct($config, $current)
    {
        $this->config = $config;
        $this->current = $current;
    }

    public function run()
    {
        if (!$manifest = $this->getManifest()) {
            throw new Exception("Unable to retrieve manifest");
        }

        $this->setLatest($manifest);

        foreach ($this->config as $type => $path) {
            if ($this->current->{$type} !== $this->{$type}) {
                echo "Download $type: " . $this->{$type} . PHP_EOL;
                $this->save($this->getDownloadUrl($this->{$type}), $path);
                $this->writeCurrent($type, $this->{$type});
            } else {
                echo "Already has $type: " . $this->{$type} . PHP_EOL;
            }
        }
    }

    protected function writeCurrent($type, $version)
    {
        $current = [];
        $file = __DIR__ . '/../current.json';
        if (file_exists($file)) {
            $current = json_decode(file_get_contents($file), true);
        }
        $current[$type] = $version;
        $data = json_encode($current, JSON_PRETTY_PRINT);
        file_put_contents($file, $data);
    }

    protected function setLatest($manifest)
    {
        $this->snapshot = $manifest->snapshot;
        $this->release = $manifest->release;
    }

    protected function getManifest()
    {
        $data = file_get_contents(self::MANIFEST_URL);
        $manifest = json_decode($data);
        if (isset($manifest->latest)) {
            return $manifest->latest;
        }

        return false;
    }

    protected function getDownloadUrl($ver)
    {
        return preg_replace('/__VER__/', $ver, self::DOWNLOAD_URL);
    }

    protected function save($download, $savePath)
    {
        $data = file_get_contents($download);
        file_put_contents($savePath, $data);
    }
}