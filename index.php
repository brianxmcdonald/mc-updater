<?php
require_once __DIR__ . '/src/Update.php';
$config = include __DIR__ . '/config.php';

$currentFile = __DIR__ . '/current.json';
$current = json_decode(json_encode(['release' => null, 'snapshot' => null]));

if (file_exists($currentFile)) {
    $current = json_decode(file_get_contents($currentFile));
}

$updater = new Update($config, $current);
$updater->run();